import React, { Component } from 'react'
import IconButton from '@material-ui/core/IconButton';
import {
    MdContentCopy
} from "react-icons/md";
class TextAreaComponent extends Component {
    constructor(props) {
        super(props)
        this.copyTextToClipboard = this.copyTextToClipboard.bind(this)
    }
    textAreaField = null
    copyTextToClipboard() {
        this.textAreaField.select()
        if (document.queryCommandSupported('copy')) {
            try {
                this.copyText(this.textAreaField)
            } catch (error) {
                console.error(error)
            }
        }
    }
    copyText(element) {
        var range, selection, worked;
        try {
            if (document.body.createTextRange) {
                range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            } else if (window.getSelection) {
                selection = window.getSelection();
                range = document.createRange();
                range.selectNodeContents(element);
                selection.removeAllRanges();
                selection.addRange(range);
            }


            document.execCommand('copy');
        } catch (error) {
            console.error(error)
        }
    }

    render() {
        return (
            <div>
                <textarea
                    className="textarea-field"
                    value={this.props.value}
                    ref={
                        textAreaField => this.textAreaField = textAreaField
                    }
                ></textarea>
                <IconButton color="primary" aria-label="upload picture" component="span" onClick={this.copyText}>
                    <MdContentCopy />
                </IconButton>
            </div>
        )
    }
}
export default TextAreaComponent