import React, { Component } from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from "@material-ui/pickers";

class TimeField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: new Date(),
    };
    this.onTimeChange = this.onTimeChange.bind(this);
  }
  onTimeChange(value) {
    this.setState({ value: value }, () => {
      this.props.onTimeChange(value, this.props.name);
    });
  }

  render() {
    return (
      <div className="time-field">
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardTimePicker
            margin="normal"
            id="time-picker"
            label={`Select ${this.props.label}`}
            ampm={false}
            value={this.state.value}
            onChange={this.onTimeChange}
            KeyboardButtonProps={{
              "aria-label": "change time",
            }}
          />
        </MuiPickersUtilsProvider>
      </div>
    );
  }
}

export default TimeField;
