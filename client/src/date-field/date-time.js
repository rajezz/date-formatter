import React, { Component } from "react";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker,
} from "@material-ui/pickers";

class DateTimeField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: new Date(),
    };
    this.onDateChange = this.onDateChange.bind(this);
  }
  onDateChange(value) {
    this.setState({ value: value }, () => {
      this.props.onDateChange(value, this.props.name);
    });
  }

  render() {
    return (
      <div className="date-field">
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDateTimePicker
            margin="normal"
            ampm={false}
            id="date-picker-dialog"
            label={`Select ${this.props.label}`}
            format="yyyy/MM/dd HH:mm"
            value={this.state.value}
            onChange={this.onDateChange}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
      </div>
    );
  }
}

export default DateTimeField;
