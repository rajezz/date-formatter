import React, { Component } from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import DateDifferencePage from '../date-differnce'
import DateAddTimePage from '../date-add-time'
import DateDisplayPage from '../date-display'

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from_date: "",
      tab_value: 1,
    };
    this.onDateChange = this.onDateChange.bind(this);
    this.handleTabChange = this.handleTabChange.bind(this);
  }

  a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  handleTabChange(event, newValue) {
    this.setState({
        ...this.state,
        tab_value: newValue,
    })
  }
  onDateChange(value, name) {}

  render() {
    return (
      <div className="page">
        <Paper square>
          <Tabs
            value={this.state.tab_value}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.handleTabChange}
            aria-label="disabled tabs example"
            centered
          >
            <Tab label="Date difference"  {...this.a11yProps(0)} />
            <Tab label="Add/Subtract time"  {...this.a11yProps(1)} />
            <Tab label="Display date"  {...this.a11yProps(2)} />
          </Tabs>
        </Paper>
        <TabPanel value={this.state.tab_value} index={0}>
          <DateDifferencePage />
        </TabPanel>
        <TabPanel value={this.state.tab_value} index={1}>
        <DateAddTimePage />
        </TabPanel>
        <TabPanel value={this.state.tab_value} index={2}>
        <DateDisplayPage />
        </TabPanel>
      </div>
    );
  }
}

export default Home;
