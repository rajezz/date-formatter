import React, { Component } from 'react';
import DateTimeField from "./date-field/date-time";
import Typography from '@material-ui/core/Typography';
import TextAreaComponent from './date-field/textarea';
import TextField from '@material-ui/core/TextField';

//CSS variables
const inputElementCSS = {
    margin: "10px",
    width: "250px",
}


class DateDisplayPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            dateField: "",
            toDateString: "",
            toISOString: "",
            toJSON: "",
            toGMTString: "",
            toUTCString: "",
            toLocaleString: "",
            toLocaleTimeString: "",
            toString: "",
        };
        this.onPropertyChange = this.onPropertyChange.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.displayDate = this.displayDate.bind(this);
    }
    onPropertyChange(value, key) {
        this.setState({
            ...this.state,
            [key]: value,
        }, () => {
            this.displayDate("date")
        });
    }
    onInputChange(event) {
        let date = new Date(event.target.value)
        if (date != "Invalid Date") {
            this.setState({
                ...this.state,
                dateField: date,
            }, () => {
                this.displayDate("dateField")
            });
        }
    }
    displayDate(field) {
        const value = this.state[field]
        const toDateString = value.toDateString()
        const toISOString = value.toISOString()
        const toJSON = value.toJSON()
        const toGMTString = value.toGMTString()
        const toUTCString = value.toUTCString()
        const toLocaleString = value.toLocaleString()
        const toLocaleTimeString = value.toLocaleTimeString()
        const toString = value.toString()
        this.setState({
            ...this.state,
            toDateString: toDateString,
            toISOString: toISOString,
            toJSON: toJSON,
            toGMTString: toGMTString,
            toUTCString: toUTCString,
            toLocaleString: toLocaleString,
            toLocaleTimeString: toLocaleTimeString,
            toString: toString,
        })
    }

    render() {
        return (
            <div>
                <DateTimeField
                    label="Date"
                    onDateChange={this.onPropertyChange}
                    name="date"
                />
                <TextField
                    id="standard-helperText"
                    label="Date"
                    value={this.state.dateField}
                    onChange={this.onInputChange}
                    style={inputElementCSS}
                    helperText="Enter date as a String" />
                {this.state.toDateString ? < TextAreaComponent value={
                    this.state.toDateString
                }
                /> : ""}
                {this.state.toISOString ? < TextAreaComponent value={
                    this.state.toISOString
                }
                /> : ""}
                {this.state.toJSON ? < TextAreaComponent value={
                    this.state.toJSON
                }
                /> : ""}
                {this.state.toGMTString ? < TextAreaComponent value={
                    this.state.toGMTString
                }
                /> : ""}
                {this.state.toUTCString ? < TextAreaComponent value={
                    this.state.toUTCString
                }
                /> : ""}
                {this.state.toLocaleString ? < TextAreaComponent value={
                    this.state.toLocaleString
                }
                /> : ""}
                {this.state.toLocaleTimeString ? < TextAreaComponent value={
                    this.state.toLocaleTimeString
                }
                /> : ""}
                {this.state.toString ? < TextAreaComponent value={
                    this.state.toString
                }
                /> : ""}
            </div>
        );
    }
}

export default DateDisplayPage;