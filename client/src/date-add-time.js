import React, { Component } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Typography from '@material-ui/core/Typography';
import DateTimeField from "./date-field/date-time";
import TimeField from "./date-field/time";
import Card from '@material-ui/core/Card';
import './App.css'

//CSS variables
const outputCSS = {
  margin: "10px !important"
}

class DateAddTimePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      time: new Date(),
      operation: "add",
      time_difference: ""
    };
    this.onPropertyChange = this.onPropertyChange.bind(this);
    this.onRadioChange = this.onRadioChange.bind(this);
    this.displayDate = this.displayDate.bind(this);

  }
  componentDidMount() {
    this.displayDate()
  }
  onPropertyChange(value, key) {
    this.setState({
      ...this.state,
      [key]: value,
    }, () => {
        this.displayDate()
    });
  }
  onRadioChange(event, key) {
    this.setState({
        ...this.state,
        [key]: event.target.value,
      }, () => {
        this.displayDate()
      });
  }
  displayDate() {
      let hour = this.state.time.getHours()
      let minute = this.state.time.getMinutes()
      let newDate = new Date(this.state.date)
      if (this.state.operation == "add") {
        newDate.setHours(newDate.getHours() + hour, newDate.getMinutes() + minute)
      } else {
        newDate.setHours(newDate.getHours() - hour, newDate.getMinutes() - minute)
      }
    this.setState({
        ...this.state,
        time_difference: newDate.toString()
      })
  }
  render() {
    return (
      <div className="tab-page">
        <DateTimeField
          label="Date"
          onDateChange={this.onPropertyChange}
          name="date"
        />
        <TimeField
          label="Time"
          onTimeChange={this.onPropertyChange}
          name="time"
        />
        <FormControl component="fieldset">
          <RadioGroup
            row
            aria-label="position"
            name="position"
            value={this.state.operation}
            onChange={(value) => this.onRadioChange(value, "operation")}
          >
            <FormControlLabel
              value="add"
              control={<Radio color="primary" />}
              label="Add"
            />
            <FormControlLabel
              value="subtract"
              control={<Radio color="primary" />}
              label="Subtract"
            />
          </RadioGroup>
        </FormControl>
        <Card className="display-output" variant="outlined">
        <Typography className={outputCSS} gutterBottom variant="h5" component="h2">
            {this.state.time_difference}
          </Typography>
          </Card>
      </div>
    );
  }
}

export default DateAddTimePage;
