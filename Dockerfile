FROM node:14

WORKDIR C:\Users\SPRITLE\Downloads\work files\personal projects\date-formatter

COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

EXPOSE 3004

CMD [ "node", "app.js" ]