module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [{
        name: "Date Formatter",
        script: "app.js",
        env: {
            COMMON_VARIABLE: "true"
        },
        env_production: {
            NODE_ENV: "production"
        },
        log_date_format: "YYYY-MM-DD HH:mm Z"
    }],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy: {
        production: {
            user: "ubuntu",
            host: "rajeswaran.tk",
            ref: "origin/master",
            repo: "git@gitlab.com:rajezz/date-formatter.git",
            path: "/home/ubuntu/projects/date-formatter",
            "post-deploy": "npm install && pm2 startOrRestart ecosystem.config.js --env production"
        }
    }
}
