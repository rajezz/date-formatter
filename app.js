var express = require('express');
var path = require('path');
var chalk = require('chalk');

var app = express();

app.use(express.static(path.join(__dirname, 'client/build')))
app.listen(3004, () => {
    console.log('%s App is running at http://localhost:3004', chalk.green('✓'));
    console.log('  Press CTRL-C to stop\n');
  });
